import React from "react";

export const Stats = (data) => {
    const base = Object.keys(data.stats.base).map(function(key) {
        const i = Math.floor((data.stats.base[key] - 10)/2);
        return (
        <div className="col-4" key={key}>
            {key}: {data.stats.base[key]}
            <div>mod: {i > 0 ? '+' + i : i}</div>
        </div>
        );
    });

    return (
        <>
            Character Stats?
            <div className="row">
                {base}
            </div>
        </>
    );
}