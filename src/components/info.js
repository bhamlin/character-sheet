import React from "react";

export const Info = (c) => {
    const characterAttr = Object.keys(c.character).map(key => 
        <li key={key}>{key}: {c.character[key]}</li>
    );

    return (
        <>
            some info about the character
            <ul>
                {characterAttr}
            </ul>
        </>
    );
}