export const characters = [
    {
        info: {
            name: 'Bearmourn',
            class: 'Fighter',
            level: 1,
            background: 'Waaagh!',
            race: 'Half Orc',
            alignment: 'Evil',
            xp: 10
        },
        stats: {
            base: {
                str: 16,
                dex: 11,
                con: 13,
                int: 8,
                wis: 9,
                chr: 10
            },
            proficiency: 2,
            savingThrows: {
                str: 5,
                dex: -1,
                con: 4,
                int: 0,
                wis: 1,
                chr: 2
            },
            skills: {
                acrobatics: -1,
                animal: 1,
                arcana: 0,
                athletics: 5,
                deception: 2,
                history: 2,
                insight: 1,
                intimidation: 2,
                investigation: 0,
                medicine: 1,
                nature: 0,
                perception: 3,
                performance: 2,
                persuasion: 4,
                religion: 0,
                slight: -1,
                stealth: -2,
                survival: 1
            },
            wisdom: 13,
            other: {
                proficiencies: ['All armor','shields', 'simple weapons', 'martial weapons'],
                languages: ['Common', 'Orcish', 'Goblin']
            }
        },
        equipment: {

        }
    }
];