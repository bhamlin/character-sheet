import React from 'react';
import ReactDOM from 'react-dom';
import {characters} from './data';
import {Info} from './components/info';
import {Stats} from './components/stats';
import './app.scss';

console.log(characters);

class CharacterSheet extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            characters: characters
        }
    }

    render() {
        const characters = this.state.characters[0];

        return (
            <div className="container">
                <h1>work in progress!</h1>
                <div className="row">
                    <div className="col-12">
                        <Info character={characters.info} />
                    </div>
                    <div className="col-12 col-md-6">
                        <Stats stats={characters.stats} />
                    </div>
                </div>
            </div>
        );
    }
}



ReactDOM.render(
    <CharacterSheet />,
    document.getElementById('root')
);